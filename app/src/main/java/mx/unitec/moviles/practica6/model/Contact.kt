package mx.unitec.moviles.practica6.model

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity (tableName =Contact.TABLE_NAME )
data class Contact(

    @ColumnInfo(name = "name")@NonNull val name: String ,
    @ColumnInfo(name = "phone")@NonNull val phone: String ,
    @ColumnInfo(name = "mail")val mail: String) {



    companion object{
        const val TABLE_NAME="contact"
    }
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name= "id")
    var id: Int=0
}