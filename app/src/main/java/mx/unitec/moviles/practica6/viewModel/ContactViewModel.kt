package mx.unitec.moviles.practica6.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import mx.unitec.moviles.practica6.model.Contact
import mx.unitec.moviles.practica6.repository.ContactRepository

class ContactViewModel(application: Application):AndroidViewModel(application) {
    private val repository = ContactRepository(application)
    val contact = repository.getContacts()
    fun saveContact(contact: Contact)= viewModelScope.launch(Dispatchers.IO) {
        repository.insert(contact)
    }


}


